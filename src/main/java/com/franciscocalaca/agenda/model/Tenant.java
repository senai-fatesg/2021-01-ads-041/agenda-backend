package com.franciscocalaca.agenda.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Tenant {

	@Id
	private String name;
	
	private String mailSmtp;
	
	private  Integer mailPort;
	
	private String mailUser;
	
	private String mailPass;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailSmtp() {
		return mailSmtp;
	}

	public void setMailSmtp(String mailSmtp) {
		this.mailSmtp = mailSmtp;
	}

	public Integer getMailPort() {
		return mailPort;
	}

	public void setMailPort(Integer mailPort) {
		this.mailPort = mailPort;
	}

	public String getMailUser() {
		return mailUser;
	}

	public void setMailUser(String mailUser) {
		this.mailUser = mailUser;
	}

	public String getMailPass() {
		return mailPass;
	}

	public void setMailPass(String mailPass) {
		this.mailPass = mailPass;
	}
	
	
}
