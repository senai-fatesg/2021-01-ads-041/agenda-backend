package com.franciscocalaca.agenda.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.agenda.dao.MunicipioDao;
import com.franciscocalaca.agenda.model.Municipio;

@RestController
@RequestMapping("/municipio")
public class MunicipioRest {
	
	@Autowired
	private MunicipioDao municipioDao;

	@GetMapping("/{nome}")
	public List<Municipio> get(@PathVariable("nome") String nome){
		return municipioDao.findByNome(nome);
	}
	
}
