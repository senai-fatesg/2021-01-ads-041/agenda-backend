package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.model.UsuarioRecuperacao;

@Repository
public interface UsuarioRecuperacaoDao extends JpaRepository<UsuarioRecuperacao, Long>{

	UsuarioRecuperacao findByHash(String hash);
	
}
