package com.franciscocalaca.agenda.bo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.franciscocalaca.agenda.dao.CepDao;
import com.franciscocalaca.agenda.dao.MunicipioDao;
import com.franciscocalaca.agenda.model.Cep;
import com.franciscocalaca.agenda.model.Municipio;
import com.franciscocalaca.http.utils.RespHttp;
import com.franciscocalaca.http.utils.UtilHttp;

@Service
public class CepBo {
	
	@Autowired
	private MunicipioDao municipioDao;
	
	@Autowired
	private CepDao cepDao;
	
	public Cep consultar(String cep) throws IOException {
		cep = cep.replaceAll("[^0-9]", "");
		
		Optional<Cep> cepObjOpt = cepDao.findById(cep);
		if(cepObjOpt.isPresent()) {
			return cepObjOpt.get();
		}else {
			String url = String.format("https://viacep.com.br/ws/%s/json/", cep);
			
			Map<String, String> headers = new HashMap<String, String>();
			RespHttp resp = UtilHttp.sendGet(url, headers, "utf-8");
			Map<String, Object> map = resp.getContentAsMap();
			
			Cep cepObj = new Cep();
			cepObj.setBairro((String) map.get("bairro"));
			cepObj.setCep(cep);
			cepObj.setLogradouro((String) map.get("logradouro"));
			String municipioId = (String) map.get("ibge");
			Municipio municipio = municipioDao.findById(Long.parseLong(municipioId)).get();
			cepObj.setMunicipio(municipio);
			cepDao.save(cepObj);
			return cepObj;
		}
		
		
	}

}
