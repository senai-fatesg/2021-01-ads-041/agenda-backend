package com.franciscocalaca.agenda.web.rest;

import java.io.InputStream;
import java.util.Map;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.agenda.dao.UsuarioDao;
import com.franciscocalaca.agenda.dao.UsuarioRecuperacaoDao;
import com.franciscocalaca.agenda.model.Usuario;
import com.franciscocalaca.agenda.model.UsuarioRecuperacao;
import com.franciscocalaca.http.auth.PasswordChange;
import com.franciscocalaca.http.auth.UtilManager;
import com.franciscocalaca.util.UtilRecurso;
import com.franciscocalaca.util.UtilStream;

@RestController
@RequestMapping("/recuperar-senha")
public class UsuarioRecuperacaoRest {
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private UsuarioRecuperacaoDao usuarioRecuperacaoDao;
	
	@Value("${ads04.auth.urlAuthManager}")
	private String urlAuthManager;
	
	@Value("${ads04.auth.user}")
	private String user;
	
	@Value("${ads04.auth.pass}")
	private String pass;
	
	@PostMapping("/solicitar")
	public void solicitar(@RequestBody String emailStr) {
		Usuario usuario = usuarioDao.findByEmail(emailStr);
		UsuarioRecuperacao rec = new UsuarioRecuperacao();
		rec.setUsuario(usuario);
		usuarioRecuperacaoDao.save(rec);
		
		InputStream is = UtilRecurso.getInputStreamRecursoDiretorio("rec-senha-mail.txt");
		byte [] bytes = UtilStream.lerInputStream(is);
		String corpo = new String(bytes);
		
		String link = "http://localhost:4200/nova-senha/" + rec.getHash();
		corpo = corpo.replace("${nome}", usuario.getNome());
		corpo = corpo.replace("${link}", link);
		
		try {
			HtmlEmail email = new HtmlEmail();
			email.setHostName(usuario.getTenant().getMailSmtp());
			email.setSmtpPort(usuario.getTenant().getMailPort());
			email.setAuthenticator(new DefaultAuthenticator(usuario.getTenant().getMailUser(), usuario.getTenant().getMailPass()));
			email.setSSLOnConnect(false);
			email.setFrom(usuario.getTenant().getMailUser(), usuario.getTenant().getName());
			email.setSubject("Recuperação de Senha");
			email.setMsg(corpo);
			email.addTo(emailStr);
			email.setHtmlMsg(corpo);
			email.send();
		} catch (EmailException e) {
			e.printStackTrace();
		}
		
		
	}

	
	@PostMapping("/confirmar")
	public void confirmar(@RequestBody Map<String, Object> data) {
		String hash = (String) data.get("hash");
		String senha = (String) data.get("senha");
		UsuarioRecuperacao rec = usuarioRecuperacaoDao.findByHash(hash);
		if(!rec.isAtivado()) {
			rec.setAtivado(true);
			usuarioRecuperacaoDao.save(rec);
			PasswordChange change = new PasswordChange();
			change.setLogin(rec.getUsuario().getLogin());
			change.setNewPass(senha);
			UtilManager.change(urlAuthManager + "/changeWitoutPass", user, pass, change);
		}else {
			//tratar uma msg adequada
		}
	}
}
